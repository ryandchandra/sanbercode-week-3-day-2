<!DOCTYPE HTML>
<html>
	<head>
		<title>Form</title>
		<meta charset="UTF-8">
	</head>
	<body>
		<h1>Buat Account Baru!</h1>
		<h3>Sign Up Form</h3>
		
		<form action="/welcome" method="POST">
			@csrf
			<label for="first-name">First name:</label><br><br>
			<input type="text" id="first-name" name="first-name"><br><br>
			<label for="last-name">Last name:</label><br><br>
			<input type="text" id="last-name" name="last-name"><br><br>
			
			<label>Gender:</label><br><br>
			<input type="radio" id="gender-male" name="gender" value="Male"><label for="gender-male">Male</label><br>
			<input type="radio" id="gender-female" name="gender" value="Female"><label for="gender-female">Female</label><br>
			<input type="radio" id="gender-other" name="gender" value="Other"><label for="gender-other">Other</label><br>
			
			<br>
			
			<label>Nationality:</label><br><br>
			<select>
				<option value="1">Indonesian</option>
				<option value="2">Singaporean</option>
				<option value="3">Malaysian</option>
				<option value="4">Australian</option>
			</select>
			
			<br><br>
			
			<label>Language Spoken:</label><br><br>
			<input type="checkbox" id="checkbox-bahasa-indonesia" name="language" value="1"><label for="checkbox-bahasa-indonesia">Bahasa Indonesia</label><br>
			<input type="checkbox" id="checkbox-english" name="language" value="2"><label for="checkbox-english">English</label><br>
			<input type="checkbox" id="checkbox-other" name="language" value="3"><label for="checkbox-other">Other</label><br>
			
			<br>
			
			<label>Bio:</label><br><br>
			<textarea cols="30" rows="10"></textarea><br>
			
			<button type="submit" name="submit">Sign Up</button>
		</form>
	</body>
</html>