<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function RegisterFormView(){
        return view('register');
    }

    public function RegisterFinished_GET(){
        return view('welcome');
    }

    public function RegisterFinished_POST(){
        return view('welcome');
    }
}
